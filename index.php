
<?php
include_once "./autoload.php";

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Development Area</title>
	<!-- ALL CSS FILES  -->
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/style.css">
	<link rel="stylesheet" href="assets/css/responsive.css">
</head>
<body>
	
	<div class="wrap-table">
		<a class="btn btn-primary btn-sm" href="./create.php">Add new student</a>
		<br>
		<form action="" class="form-inline" method="POST">
			<div class="my-3">
			 <select class="form-control" name="ls" id="">
					<option value="">--Select--</option>
					<option value="Uttara">Uttara</option>
					<option value="Mirpur">Mirpur</option>
					<option value="Badda">Badda</option>
					<option value="Banani">Banani</option>
					<option value="Gulshan">Gulshan</option>
					<option value="Farmgate">Farmgate</option>
					<option value="Malibag">Malibag</option>
			  </select>

			</div>

			<div class="my-3">
			<select class="form-control" name="edu" id="">
					<option value="">--Select--</option>
					<option value="CSE">CSE</option>
					<option value="EEE">EEE</option>
					<option value="BBA">BBA</option>
					<option value="MBA">MBA</option>
					<option value="CIVIL">CIVIL</option>
					<option value="ENGLISH">ENGLISH</option>
					<option value="LAW">LAW</option>
			</select>

			</div>

			<div class="my-3">
				<input type="submit" name="sub" value="Submit" class="btn btn-primary">
			</div>

			<!-- <div class="input-group text-right">
				<input type="search" name="search" class="form-control rounded" placeholder="Search" aria-label="Search" aria-describedby="search-addon" />
				<button type="button" class="btn btn-primary">search</button>
			</div> -->

		</form>
		<br>
		<div class="card shadow-sm">
			<div class="card-body">
				<h2>All Data</h2>
				<table class="table table-striped">
					<thead>
						<tr>
							<th>#</th>
							<th>Name</th>
							<th>Email</th>
							<th>Cell</th>
							<th>Age</th>
							<th>Gender</th>
							<th>Eduction</th>
							<th>Payment</th>
							<th>Location</th>
							<th>Photo</th>
							<th width:400 >Action</th>
						</tr>
					</thead>
					<tbody>

					<?php

					//Get all student data

					$data = connect() -> query("SELECT *FROM students ORDER BY name ASC" );

					// $data = connect() -> query("SELECT *FROM students WHERE payment IS NOT NULL " ); ekhane jara payment koe nai tader data gula dekhabe.
					// $data = connect() -> query("SELECT *FROM students WHERE location IN('Badda', 'Banani')" );
					// $data = connect() -> query("SELECT *FROM students WHERE location='badda' OR location='banani'" );
					// $data = connect() -> query("SELECT *FROM students WHERE age BETWEEN 15 AND 25 ORDER BY age ASC" );
					// $data = connect() -> query("SELECT *FROM students WHERE NOT education='EEE'" );
					// $data = connect() -> query("SELECT *FROM students WHERE gender='Female || location='mirpur' " );

					/**
					 * $data = connect() -> query("SELECT *FROM students LIMIT 4" ); ekhane prothom 4 ta column show korbe table theke.. 
					 * shudhu LIMIT dile ekta column show korbe.
					 */ 


					 //Payment

					$minmax = connect() -> query("SELECT MIN(payment) as minPay, MAX(payment) as maxpay, SUM(payment)as
					total, AVG(payment) as avgpay FROM students " );

					$minmaxdata = $minmax -> fetch_object();

					 //search student by loaction and education


					 if(isset($_POST['sub'])){

						$location = $_POST['ls'];
						$edu = $_POST['edu'];
						$data = connect() -> query("SELECT *FROM students WHERE location = '$location' OR education = '$edu' " );

					 }

                    //search 
					//  if(isset($_POST['search'])){

					//     $search = $_POST['search'];
					// 	$data = connect() -> query("SELECT *FROM students WHERE name LIKE '%$search%'" );

					//  }



                    $i = 1;

					while( $student = $data -> fetch_object()):
				
					?>
						<tr>
							<td><?php echo $i; $i++;?></td>
							<td><?php echo $student -> name;?></td>
							<td><?php echo $student -> email;?></td>
							<td><?php echo $student -> cell;?></td>
							<td><?php echo $student -> age;?></td>
							<td><?php echo $student -> gender;?></td>
							<td><?php echo $student -> education;?></td>
							<td><?php echo $student -> payment;?></td>
							<td><?php echo $student -> location;?></td>
							<td><img src="photos/<?php echo $student -> photo;?>" alt=""></td>
							<td>
								<a class="btn btn-sm btn-info" href="#">View</a>
								<a class="btn btn-sm btn-warning" href="#">Edit</a>
								<a class="btn btn-sm btn-danger" href="#">Delete</a>
							</td>
						</tr>

						<?php endwhile;?>

						<!-- <tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td>
								Total: <?php echo $minmaxdata -> total;?>
							</td>
						</tr> -->

					</tbody>
				</table>
			</div>
		</div>
	</div>
	







	<!-- JS FILES  -->
	<script src="assets/js/jquery-3.4.1.min.js"></script>
	<script src="assets/js/popper.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/custom.js"></script>
</body>
</html>