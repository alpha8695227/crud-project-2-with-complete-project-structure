
<?php
include_once "./autoload.php";

?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Development Area</title>
	<!-- ALL CSS FILES  -->
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/style.css">
	<link rel="stylesheet" href="assets/css/responsive.css">
</head>
<body>


<?php

/**
 * student form data
 */

if(isset($_POST['student_add'])){

#get form values
// jehetu data gulu dhorte parci,

$name = $_POST ['name'];
$email = $_POST ['email'];
$cell = $_POST ['cell'];
$age= $_POST['age'];
$gender = $_POST ['gender'] ?? "";
$education = $_POST ['education'];
$location = $_POST ['location'];
}

/**
 * Form validation
 */

if (empty($name) || empty($email) || empty($cell)) {

	$msg = validation('All fields are requied!');
	
}else {

	//Get ile info

	$file_name = time(). rand(). $_FILES['photo']['name'];
	$file_tmp_name = $_FILES['photo']['tmp_name'];


	move_uploaded_file($file_tmp_name, 'photos/'. $file_name);

	connect() -> query("INSERT INTO students (name, email, cell, gender, education, location, photo) VALUES ('$name', '$email', '$cell', '$gender', '$education', '$location', '$file_name') " );

	$msg = $msg = validation('Data Stable!', 'success');


} 

?>

	<div class="wrap">
	<a class="btn btn-primary btn-sm" href="./index.php">All students</a>
		<br>
		<br>
		<div class="card shadow-sm">
			<div class="card-body">
				<h2>Create new student</h2>
				<?php echo $msg ?? '';?>
				<form action="" method="POST" enctype="multipart/form-data">
					<div class="form-group">
						<label for="">Name</label>
						<input name="name" class="form-control" type="text">
					</div>
					<div class="form-group">
						<label for="">Email</label>
						<input name="email" class="form-control" type="text">
					</div>
					<div class="form-group">
						<label for="">Cell</label>
						<input name="cell" class="form-control" type="text">
					</div>
					<div class="form-group">
						<label for="">Age</label>
						<input name="age" class="form-control" type="text">
					</div>
					<div class="form-group">
						<label for="">Gender</label>
						<input name="gender" class="" type="radio" value="Male" id="Male"><label for="Male">Male</label>
						<input name="gender" class="" type="radio" value="Female" id="Female"><label for="Female">Female</label>
					</div>
					<div class="form-group">
						<label for="">Education</label>
						<select class="form-control" name="education" id="">
                                <option value="">--Select--</option>
                                <option value="CSE">CSE</option>
                                <option value="EEE">EEE</option>
                                <option value="BBA">BBA</option>
                                <option value="MBA">MBA</option>
                                <option value="CIVIL">CIVIL</option>
                                <option value="ENGLISH">ENGLISH</option>
                                <option value="LAW">LAW</option>
                        </select>
					</div>
					<div class="form-group">
						<label for="">Location</label>
						<select class="form-control" name="location" id="">
                                <option value="">--Select--</option>
                                <option value="Uttara">Uttara</option>
                                <option value="Mirpur">Mirpur</option>
                                <option value="Badda">Badda</option>
                                <option value="Banani">Banani</option>
                                <option value="Gulshan">Gulshan</option>
                                <option value="Farmgate">Farmgate</option>
                                <option value="Malibag">Malibag</option>
                            </select>
					</div>
					<div class="form-group">
						<label for="">Photo</label>
						<br>
						<img style="max-width: 100%;" id="preview" src="">
						<br> 
						<input name="photo" class="d-none" type="file" id="image-upload">
						<label for="image-upload"><img id="preview" style="height: 70px; width:70px; display:block; cursor:pointer" src="https://static.thenounproject.com/png/212328-200.png"></label>
					</div>
					<div class="form-group">
						<input name="student_add" class="btn btn-primary" type="submit" value="Sign Up">
					</div>
				</form>
			</div>
		</div>
	</div>
	
<p></p>
<p></p>






	<!-- JS FILES  -->
	<script src="assets/js/jquery-3.4.1.min.js"></script>
	<script src="assets/js/popper.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/custom.js"></script>

	<script>
    $('#image-upload').change(function(e){
    let file = URL.createObjectURL(e.target.files[0]);
    $('#preview').attr('src', file);

    });
</script>


</body>
</html>


   


